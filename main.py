import random

score = 0
python_quizzes = {
    "What is the capital of France?": {
        "suggested_answers": ["Paris", "Marseille", "Lyon", "Toulouse"],
        "correct_answer": "Paris",
        "hints": [
            "The city is known as the city of love",
            "One of the most known tower is located there",
            "It starts with a P"
        ]
    },

    "Which planet is known as the 'Red Planet'?": {
        "suggested_answers": ["Venus", "Mars", "Jupiter", "Saturn"],
        "correct_answer": "Mars",
        "hints": [
            "It is the fourth planet from the Sun",
            "Named after the Roman god of war",
            "Often referred to as the 'Red Planet' due to its reddish appearance"
        ]
    },
}

# Shuffle the list before quiz starts
shuffled_list = list(python_quizzes.items())
random.shuffle(shuffled_list)
shuffled_qa_dict = dict(shuffled_list)
user_selected_answer = ''
user_question_answers = []


# recursive function that calls itself
def check_answer(answers_list, hints, number_of_trials):
    global score
    global user_selected_answer
    user_selected_answer = input("Your answer: ").strip()

    if not user_selected_answer:
        number_of_trials += 1
        if number_of_trials < 4:
            print("Incorrect answer. Please try again.")
            print(hints[number_of_trials - 1])
            check_answer(answers_list, hints, number_of_trials)
        else:
            print("Incorrect. Please move on.")
        return

    if user_selected_answer in answers_list:
        print("Correct!")

        score += 1
    else:
        number_of_trials += 1
        if number_of_trials < 4:
            print("Incorrect answer. Please try again.")
            print(hints[number_of_trials - 1])
            check_answer(answers_list, hints, number_of_trials)
        else:
            print("Incorrect. Please move on.")


def prepare_for_file_save(question, question_info, user_answer):
    user_question_answer = {
        'question': question,
        'user_answer': user_answer,
        'correct_answer': question_info['correct_answer']
    }
    user_question_answers.append(user_question_answer)


def ask_question(question, shuffled_dict):
    global score
    number_of_trials = 0
    question_info = shuffled_dict.get(question)
    if question_info:
        suggested_answers = question_info["suggested_answers"]
        correct_answer = question_info["correct_answer"]
        hints = question_info["hints"]
        print("Question:", question)

        print("Suggested answers: ")
        for answer in suggested_answers:
            print(answer)

        check_answer(correct_answer, hints, number_of_trials)
        prepare_for_file_save(question, question_info, user_selected_answer)

    else:
        print("Question not found in the dictionary.")


# Ask user to input answer for question
for index, quiz in enumerate(shuffled_qa_dict):
    ask_question(quiz, shuffled_qa_dict)

    # Appending an empty line
    print()

# Print the final score to the user
print("Your score is: " + str(score) + " out of " + str(len(shuffled_qa_dict)))
print("Thank you for your participation")
percentage_score = (score / len(shuffled_qa_dict)) * 100
print((format(percentage_score) + '%'))

with open("user_answers.txt", "w") as file:
    file.write("Your score is: " + str(score) + " out of " +
               str(len(shuffled_qa_dict)) + "\n")
    file.write("Percentage score: " + format(percentage_score) + "%\n\n")
    file.write("Your answers:\n")
    for question_answer in user_question_answers:
        file.write(question_answer['question'] + "\n")
        file.write("Your answer: " + question_answer["user_answer"] + "\n")
        file.write("Correct answer: " +
                   question_answer["correct_answer"] + "\n")
        file.write("\n")

print("User answers and score written to user_answers.txt")
