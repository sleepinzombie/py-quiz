# PY Quiz

This is a simple Python app to test the basic knowledge of Python.

# What is the app about?

It is a simple quiz console app which will provide the user with a list of questions and the user will have to reply to each one of them.

# How does it work?

- The user will be presented with a question for which he will have to reply.
- If the answer is not the correct answer, the app will provide hints to the user.
- If 3 hints are provided and the user still does not answer correctly, the current question wii be counted as a wrong answer.
- At the end of the quiz, total score will be provided to the user.
- Total score, questions and answers will be saved to a .txt file as well.
